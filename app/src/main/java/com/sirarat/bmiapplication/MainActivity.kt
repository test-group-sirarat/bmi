package com.sirarat.bmiapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import com.google.android.material.textfield.TextInputEditText
import com.sirarat.bmiapplication.databinding.ActivityMainBinding
import java.text.NumberFormat

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calculatorButton.setOnClickListener { calculateBMI() }
    }


    private fun displayBmi(bmi: Double) {
        val formattedTip = NumberFormat.getCurrencyInstance().format(bmi)
        binding.bmiResult.text = getString(R.string.bmi_Result, formattedTip)
    }
    private fun displayBody(body: String) {
        binding.idealBody.text = body
    }



    private fun calculateBMI() {
        var weight = findViewById<TextInputEditText>(R.id.weight_edit_text)
        var height = findViewById<TextInputEditText>(R.id.height_edit_text)


        var weightValue = weight.text.toString().toDouble()
        var heightValue = height.text.toString().toDouble()
        var h1 = heightValue / 100
        var h2 = h1 * h1
        var bmi = weightValue / h2

        displayBmi(bmi)



        var body = " "
         if(bmi < 18 ){
            body = "underweight"
        }
        if(bmi >= 18.5 && bmi <= 24.99 ){
            body = "Normal"
        }
        if(bmi >= 25.0 && bmi <= 29.99 ){
            body = "Overweight"
        }
        if (bmi >= 30 && bmi <= 34.99){
            body = "Obesity"
    }
        if (bmi > 35){
            body = "Extremly obese"
        }
        displayBody(body)


    }

}









